from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('tweeted', views.tweeted, name='tweeted'),
    path('liked/<int:tweet_id>', views.liked, name='liked'),
    path('retweeted/<int:tweet_id>', views.retweeted, name='retweeted')
]

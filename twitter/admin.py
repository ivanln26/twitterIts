# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

# Register your models here.
class RetweetInline(admin.TabularInline):
    model = Retweet
    extra = 0

class LikeInline(admin.TabularInline):
    model = Like
    extra = 0

class TweetAdmin(admin.ModelAdmin):
    def likes_counter(self, obj):
        if obj:
            return obj.my_likes.filter(is_valid=True).count()

    def retweets_counter(self, obj):
        if obj:
            return obj.my_retweets.count()

    list_display = ('id', 'user', 'datetime', 'content')
    list_filter = ('datetime', 'user')
    search_fields = ('user', 'content')
    readonly_fields = ('likes_counter', 'retweets_counter', 'datetime')

    inlines = [LikeInline, RetweetInline]

admin.site.register(Tweet, TweetAdmin)
admin.site.register(Follow)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings

# Create your models here.
class Follow(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, unique=True, related_name='user', on_delete=models.CASCADE)
    follows = models.ManyToManyField('self', related_name='follows')

class Tweet(models.Model):
    content = models.CharField(max_length=140, null=False, blank=False)
    datetime = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='my_tweets', on_delete=models.CASCADE)

    content.verbose_name = 'Contenido'
    user.verbose_name = 'Usuario'

    def __str__(self):
        return "Tweet {}: {} {}".format(self.id, self.user.username, str(self.datetime))

    def getLikesCount(self):
        return self.my_likes.filter(is_valid=True).count()

    def getRetweetsCount(self):
        return self.my_retweets.count()


class Like(models.Model):
    datetime = models.DateTimeField(auto_now_add=True)
    is_valid = models.BooleanField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    tweet = models.ForeignKey('Tweet', related_name='my_likes', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Like'
        verbose_name_plural = 'Likes'

    def __str__(self):
        return "{} liked {}'s tweet".format(self.user.username, self.tweet.user.username)

class Retweet(models.Model):
    datetime = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    tweet = models.ForeignKey('Tweet', related_name='my_retweets', on_delete=models.CASCADE)

    def __str__(self):
        return "{} retweeted {}'s tweet.".format(self.user.username, self.tweet.user.username)

    class Meta:
        verbose_name = 'Retweet'
        verbose_name_plural = 'Retweets'

from django import forms
from django.db import models
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm, Textarea, TextInput
from .models import Tweet, Like

class TweetForm(ModelForm):
    class Meta:
        model = Tweet
        fields = ['content']
        widgets = {'content': TextInput(attrs={'class': 'materialize-textarea validate', 'style': 'width: 85%;'})}
        labels = {'content': "What's happening?"}

class LikeForm(ModelForm):
    class Meta:
        model = Like
        fields = ['is_valid']

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1',
            'password2']

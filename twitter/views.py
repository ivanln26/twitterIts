# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.shortcuts import (render, redirect)
from django.contrib.auth import get_user_model, login, authenticate
from django.contrib.auth.forms import UserCreationForm
from .models import *
from .forms import TweetForm, LikeForm, SignUpForm

# Create your views here.
def index(request):
    tweets = Tweet.objects.order_by('datetime')[::-1] # [empieza : termina : salto]
    users = get_user_model().objects.all()
    likes = Like.objects.all()
    retweets = Retweet.objects.all()
    form = TweetForm()
    like_form = LikeForm()
    ctx = {'tweets': tweets, 'users': users, 'likes': likes,
        'retweets': retweets, 'form': form, 'like_form': like_form}
    return render(request, 'core/home.html', ctx)

def tweeted(request):
    content = request.POST['content']
    tweet = Tweet(user=request.user, content=content)
    tweet.save()
    return redirect('index')

def liked(request, tweet_id):
    tweet = Tweet.objects.get(id=tweet_id)
    try:
        like = Like.objects.get(user=request.user, tweet=tweet)
        if like.is_valid:
            like.is_valid = False
        else:
            like.is_valid = True
        like.save()
        return redirect('index')
    except ObjectDoesNotExist:
        like = Like(is_valid=True, user=request.user, tweet=tweet)
        like.save()
        return redirect('index')

def retweeted(request, tweet_id):
    tweet = Tweet.objects.get(id=tweet_id)
    try:
        retweet = Retweet.objects.get(user=request.user, tweet=tweet)
        retweet.delete()
    except ObjectDoesNotExist:
        retweet = Retweet(user=request.user, tweet=tweet)
        retweet.save()
    return redirect('index')

def signup(request):
    if request.method == 'POST':
        su_form = SignUpForm(request.POST)
        if su_form.is_valid():
            su_form.save()
            username = su_form.cleaned_data.get('username')
            raw_password = su_form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        su_form = SignUpForm()
    return render(request, 'core/signup.html', {'su_form': su_form})
